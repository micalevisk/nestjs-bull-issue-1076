import { Module } from '@nestjs/common';
import { BullModule, Processor, OnQueueError } from '@nestjs/bull';

@Processor('audio')
export class AudioConsumer {
  @OnQueueError()
  onError(err: Error) {
    console.error(err)
  }
}

@Module({
  imports: [
    BullModule.forRoot({
      redis: {
        host: 'foo',
        port: 1234
      },
    }),

    BullModule.registerQueue({
      name: 'audio',
    })
  ],
  providers: [AudioConsumer],
})
export class AppModule {}
